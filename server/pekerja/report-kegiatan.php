<?php
require_once('../koneksi.php');
require_once('../mpdf/mpdf.php');
if(!isset($_GET['awal']) || !isset($_GET['akhir']) ) {
  http_response_code(500);
  echo json_encode(array('success' => false, 'msg' => 'Tanggal Tidak Lengkap'));
  return;
}
$mpdf = new mPDF('utf-8', 'A4', 10.5, 'arial');
$awal = $_GET['awal'];
$akhir = $_GET['akhir'];

if(isset($_GET['pekerja'])) {
  $pekerja = $_GET['id_user'];
  $sql = "SELECT * FROM datapekerja LEFT JOIN biodatapekerja ON datapekerja.ID_USER = biodatapekerja.id WHERE datapekerja.ID_USER = '$pekerja' AND DATE(datapekerja.TANGGAL) BETWEEN DATE('$awal') AND DATE('$akhir')";
  $sqlPeminjaman = "SELECT peminjaman_alat.*, biodatapekerja.nama_lengkap FROM peminjaman_alat LEFT JOIN biodatapekerja ON peminjaman_alat.id_user = biodatapekerja.id WHERE peminjaman_alat.id_user = '$pekerja' AND DATE(datapekerja.TANGGAL) BETWEEN DATE('$awal') AND DATE('$akhir')";
} else {
  $sql = "SELECT * FROM datapekerja LEFT JOIN biodatapekerja ON datapekerja.ID_USER = biodatapekerja.id WHERE DATE(TANGGAL) BETWEEN DATE('$awal') AND DATE('$akhir')";
  $sqlPeminjaman = "SELECT peminjaman_alat.*, biodatapekerja.nama_lengkap, alat.NAMA_ALAT FROM peminjaman_alat LEFT JOIN alat ON peminjaman_alat.id_alat = alat.ID_ALAT LEFT JOIN biodatapekerja ON peminjaman_alat.id_user = biodatapekerja.id WHERE DATE(peminjaman_alat.tanggal) BETWEEN DATE('$awal') AND DATE('$akhir')";
}
// $sql = "SELECT * FROM datapekerja LEFT JOIN biodatapekerja ON datapekerja.ID_USER = biodatapekerja.id WHERE DATE(TANGGAL) BETWEEN DATE('$awal') AND DATE('$akhir')";
$run = mysql_query($sql);
$runAlat = mysql_query($sqlPeminjaman);
$result = null;
$resultAlat = null;
if($run && $runAlat) {
  while($row = mysql_fetch_assoc($run)) {
    $result .= "
      <tr style='border: 1px solid;'>
        <td style='border: 1px solid;'>".$row['TANGGAL']."</td>
        <td style='border: 1px solid;'>".$row['nama_lengkap']."</td>
        <td style='border: 1px solid;'>".$row['KEGIATAN']."</td>
        <td style='border: 1px solid;'>".$row['JAM_MASUK']." Hari</td>
        <td style='border: 1px solid;'>".$row['JAM_KELUAR']."</td>
        <td style='border: 1px solid;'>".$row['DOSIS_AWAL']."</td>
        <td style='border: 1px solid;'>".$row['DOSIS_AKHIR']."</td>
        <td style='border: 1px solid;'>".$row['RUANGAN']."</td>
        <td style='border: 1px solid;'>".$row['TYPE_DOSE']."</td>
      </tr>
    ";
  }

  while($rowAlat = mysql_fetch_assoc($runAlat)) {
    $resultAlat .= "
      <tr style='border: 1px solid;'>
        <td style='border: 1px solid;'>".$rowAlat['tanggal']."</td>
        <td style='border: 1px solid;'>".$rowAlat['nama_lengkap']."</td>
        <td style='border: 1px solid;'>".$rowAlat['NAMA_ALAT']."</td>
        <td style='border: 1px solid;'>".$rowAlat['jumlah_alat']."</td>
      </tr>
    ";
  }
} else {
  http_response_code(500);
  echo json_encode(array('success' => false, 'err' => mysql_error()));
}
ob_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <!-- <link rel="stylesheet" href="http://localhost/atk/components/css/bootstrap.min.css">
  <link rel="stylesheet" href="http://localhost/atk/components/css/font-awesome.min.css"> -->
  <title>Laporan Kegiatan</title>
  <style>
    .header img {
      margin-top: 5px;
      margin-left: 10px;
      float: left;
    }

    .header-text {
      margin-top: 25px;
      text-align: center;
      clear: right;
    }

    .tgl {
      float: right;
      margin-top: 5px;
    }

    .content {
      clear: both;
    }

    table {
      width: 100%;
      margin-left: 0%;
    }

    table td {
      text-align: center;
      padding: 10px 5px;
    }
  </style>
</head>
<body>
  <div class="header">
    <img src="../../assets/logo.png" alt="LOGO" width="60px" height="60px">
  </div>
  <div class="header-text">
    <h2>LAPORAN KEGIATAN</h2>
  </div>
  <hr>
  <p>PERIODE: <?php echo $_GET['awal']." Sampai ".$_GET['akhir']; ?></p>
  <hr>
  <div class="content">
    <p>Perihal: Laporan Kegiatan</p>
    <br>
    <br>
    <h4>Laporan Kegiatan</h4>
    <table style='border: 1px solid;'>
      <thead>
        <tr style='border: 1px solid;'>
          <th style='border: 1px solid;'>TANGGAL</th>
          <th style='border: 1px solid;'>NAMA PEKERJA</th>
          <th style='border: 1px solid;'>KEGIATAn</th>
          <th style='border: 1px solid;'>JAM MASUK</th>
          <th style='border: 1px solid;'>JAM KELUAR</th>
          <th style='border: 1px solid;'>DOSIS AWAL</th>
          <th style='border: 1px solid;'>DOSIS AKHIR</th>
          <th style='border: 1px solid;'>RUANGAN</th>
          <th style='border: 1px solid;'>TYPE DOSE</th>
        </tr>
      </thead>
      <tbody>
      <?php
        echo $result;
      ?>
      </tbody>
    </table>
    <hr>
    <h4>Laporan Peminjaman Alat</h4>
    <table style="border: 1px solid">
      <thead>
        <tr style='border: 1px solid;'>
          <th style='border: 1px solid;'>TANGGAL</th>
          <th style='border: 1px solid;'>NAMA PEKERJA</th>
          <th style='border: 1px solid;'>NAMA ALAT</th>
          <th style='border: 1px solid;'>JUMLAH ALAT</th>
        </tr>
      </thead>
      <tbody>
      <?php
        echo $resultAlat;
      ?>
      </tbody>
    </table>
  </div>
  <br><br>
</body>
</html>

<?php
$html = ob_get_contents();
ob_end_clean();
$mpdf->WriteHTML(utf8_encode($html));
$mpdf->Output('laporan_kegiatan_'.$awal.'_sampai_'.$akhir.'.pdf', 'I');
?>