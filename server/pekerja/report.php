<?php
require_once('../koneksi.php');
require_once('../mpdf/mpdf.php');
$mpdf = new mPDF('utf-8', 'A4', 10.5, 'arial');
$NAMA = '';
$ID_USER = '';
$TANGGAL = '';


if(isset($_GET['NAMA'])) {
  $NAMA = $_GET['NAMA'];
}

if(isset($_GET['tanggal'])) {
  $TANGGAL = $_GET['tanggal'];
}


if(isset($_GET['user']) && $_GET['user'] !== '') {
  $ID_USER = $_GET['user'];
  $sql = "SELECT * FROM datapekerja LEFT JOIN biodatapekerja ON datapekerja.ID_USER = biodatapekerja.id WHERE datapekerja.ID_USER = '$ID_USER' AND biodatapekerja.nama_lengkap LIKE '%$NAMA%' AND datapekerja.tanggal LIKE '%$TANGGAL%'";
} else {
  $sql = "SELECT * FROM datapekerja LEFT JOIN biodatapekerja ON datapekerja.ID_USER = biodatapekerja.id WHERE biodatapekerja.nama_lengkap LIKE '%$NAMA%' AND datapekerja.tanggal LIKE '%$TANGGAL%'";
}

// $sql = "SELECT * FROM datapekerja LEFT JOIN biodatapekerja ON datapekerja.ID_USER = biodatapekerja.id WHERE biodatapekerja.nama_lengkap LIKE '%$NAMA%' LIMIT $limit OFFSET $skip";
$run = mysql_query($sql);
$result = null;
if($run) {
  while($row = mysql_fetch_assoc($run)) {
    $result .= "
      <tr style='border: 1px solid'>
        <td style='border: 1px solid'>".$row['ID_USER']."</td>
        <td style='border: 1px solid'>".$row['TANGGAL']."</td>
        <td style='border: 1px solid'>".$row['nama_lengkap']."</td>
        <td style='border: 1px solid'>".$row['KEGIATAN']."</td>
        <td style='border: 1px solid'>".$row['JAM_MASUK']."</td>
        <td style='border: 1px solid'>".$row['JAM_KELUAR']."</td>
        <td style='border: 1px solid'>".$row['DOSIS_AWAL']."</td>
        <td style='border: 1px solid'>".$row['DOSIS_AKHIR']."</td>
      </tr>
    ";
  }
} else {
  http_response_code(500);
  echo json_encode(array('success' => false, 'err' => mysql_error()));
}

ob_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <!-- <link rel="stylesheet" href="http://localhost/atk/components/css/bootstrap.min.css">
  <link rel="stylesheet" href="http://localhost/atk/components/css/font-awesome.min.css"> -->
  <title>Laporan Data Pekerja</title>
  <style>
    .header {
      margin: 0 auto;
    }

    .header-text {
      margin-top: 25px;
      text-align: center;
      clear: right;
    }

    .tgl {
      float: right;
      margin-top: 5px;
    }

    .content {
      clear: both;
    }

    table {
      width: 100%;
      margin-left: 0%;
    }

    table td {
      text-align: center;
      padding: 10px 5px;
    }
  </style>
</head>
<body>
  <div class="header">
    <img src="../../assets/logo.png" alt="LOGO" width="60px" height="60px">
  </div>
  <div class="header-text">
    <h2>LAPORAN PEKERJA</h2>
  </div>
  <hr>
  <div class="content">
    <p>Perihal: Laporan Pekerja</p>
    <br>
    <table style='border: 1px solid'>
      <thead>
        <tr style='border: 1px solid'>
          <th style='border: 1px solid'>ID USER</th>
          <th style='border: 1px solid'>TANGGAL</th>
          <th style='border: 1px solid'>NAMA LENGKAP</th>
          <th style='border: 1px solid'>KEGIATAn</th>
          <th style='border: 1px solid'>JAM MASUK</th>
          <th style='border: 1px solid'>JAM KELUAR</th>
          <th style='border: 1px solid'>DOSIS AWAL</th>
          <th style='border: 1px solid'>DOSIS AKHIR</th>
        </tr>
      </thead>
      <tbody>
      <?php
        echo $result;
      ?>
      </tbody>
    </table>
  </div>
  <br><br>
</body>
</html>

<?php
$html = ob_get_contents();
ob_end_clean();
$mpdf->WriteHTML(utf8_encode($html));
$mpdf->Output('laporan_pekerja.pdf', 'I');
?>