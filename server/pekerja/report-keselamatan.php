<?php
require_once('../koneksi.php');
require_once('../mpdf/mpdf.php');
require_once('../PHPMailer/PHPMailerAutoload.php');
// require_once('../PHPMailer/src/PHPMailer.php');

if(!isset($_GET['awal']) || !isset($_GET['akhir']) ) {
  http_response_code(500);
  echo json_encode(array('success' => false, 'msg' => 'Tanggal Tidak Lengkap'));
  return;
}
$mpdf = new mPDF('utf-8', 'A4', 10.5, 'arial');
$awal = $_GET['awal'];
$akhir = $_GET['akhir'];
$pekerja = $_GET['pekerja'];

$sql = "SELECT * FROM datapekerja LEFT JOIN biodatapekerja ON datapekerja.ID_USER = biodatapekerja.id WHERE datapekerja.ID_USER = '$pekerja' AND DATE(TANGGAL) BETWEEN DATE('$awal') AND DATE('$akhir')";

$run = mysql_query($sql);

$sqlTotal = mysql_query("SELECT SUM(DOSIS_AKHIR) as 'radiasi' FROM datapekerja WHERE datapekerja.ID_USER = '$pekerja' AND DATE(TANGGAL) BETWEEN DATE('$awal') AND DATE('$akhir')");
$resultTotal = mysql_fetch_assoc($sqlTotal);
$result = null;
$np = '';
if($run) {
  while($row = mysql_fetch_assoc($run)) {
    $np = $row['nama_lengkap'];
    $result .= "
      <tr style='border: 1px solid'>
        <td style='border: 1px solid'>".$row['TANGGAL']."</td>
        <td style='border: 1px solid'>".$row['KEGIATAN']."</td>
        <td style='border: 1px solid'>".$row['JAM_MASUK']."</td>
        <td style='border: 1px solid'>".$row['JAM_KELUAR']."</td>
        <td style='border: 1px solid'>".$row['DOSIS_AWAL']."</td>
        <td style='border: 1px solid'>".$row['DOSIS_AKHIR']."</td>
        <td style='border: 1px solid'>".$row['RUANGAN']."</td>
        <td style='border: 1px solid'>".$row['TYPE_DOSE']."</td>
      </tr>
    ";
  }
} else {
  http_response_code(500);
  echo json_encode(array('success' => false, 'err' => mysql_error()));
}
$mail = new PHPMailer;
// Konfigurasi SMTP
$mail->isSMTP();
$mail->Host = 'smtp.gmail.com';
$mail->SMTPAuth = true;
$mail->Username = 'dat7joee@gmail.com';
$mail->Password = 'linuxer7';
$mail->SMTPSecure = 'ssl';
$mail->Port = 465;
$mail->setFrom('info@batan.go.id', 'BATAN');
// $mail->addReplyTo('info@batan.go.id', 'BATAN');
// Menambahkan penerima
$mail->addAddress('jois@klikfix.com');
// Menambahkan cc atau bcc 
// $mail->addCC('cc@contoh.com');
// $mail->addBCC('bcc@contoh.com');
// Subjek email
$mail->Subject = 'Laporan Keselamatan ';
// Mengatur format email ke HTML
$mail->isHTML(true);
// Konten/isi email
// $mailContent = "<h1>Mengirim Email HTML menggunakan SMTP di PHP</h1>
//     <p>Ini adalah email percobaan yang dikirim menggunakan email server SMTP dengan PHPMailer.</p>";
// $mail->Body = $mailContent;
ob_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <!-- <link rel="stylesheet" href="http://localhost/atk/components/css/bootstrap.min.css">
  <link rel="stylesheet" href="http://localhost/atk/components/css/font-awesome.min.css"> -->
  <title>Laporan Keselamatan <?php echo $np; ?></title>
  <style>
    .header {
      margin: 0 auto;
    }

    .header-text {
      margin-top: 25px;
      text-align: center;
      clear: right;
    }

    .tgl {
      float: right;
      margin-top: 5px;
    }

    .content {
      clear: both;
    }

    table {
      width: 100%;
      margin-left: 0%;
    }

    table td {
      text-align: center;
      padding: 10px 5px;
    }
  </style>
</head>
<body>
  <div class="header">
    <img src="../../assets/logo.png" alt="LOGO" width="60px" height="60px">
  </div>
  <div class="header-text">
    <h2>LAPORAN KESELAMATAN</h2>
  </div>
  <hr>
  <p>PERIODE: <?php echo $_GET['awal']." Sampai ".$_GET['akhir']; ?></p>
  <hr>
  <div class="content">
    <p>Perihal: Laporan Keselamatan</p>
    <br>
    <br>
    <h4>Nama Pekerja: <?php echo $np; ?></h4>
    <h4>TOTAL RADIASI : <?php echo $resultTotal['radiasi']; ?></h4>
    <h2 style="text-align: center;">STATUS RADIASI: <?php echo (int) $resultTotal >= 2000 ? "<strong style='color: red;'>TIDAK AMAN!</strong>" : "<strong style='color: green;'>AMAN!</strong>" ?></h2>
    <table style='border: 1px solid'>
      <thead>
        <tr style='border: 1px solid'>
          <th style='border: 1px solid'>TANGGAL</th>
          <th style='border: 1px solid'>KEGIATAn</th>
          <th style='border: 1px solid'>JAM MASUK</th>
          <th style='border: 1px solid'>JAM KELUAR</th>
          <th style='border: 1px solid'>DOSIS AWAL</th>
          <th style='border: 1px solid'>DOSIS AKHIR</th>
          <th style='border: 1px solid'>RUANGAN</th>
          <th style='border: 1px solid'>TYPE DOSE</th>
        </tr>
      </thead>
      <tbody>
      <?php
        echo $result;
      ?>
      </tbody>
    </table>
  </div>
  <br><br>
</body>
</html>

<?php
$html = ob_get_contents();
ob_end_clean();
$mail->SMTPOptions = array(
  'ssl' => array(
      'verify_peer' => false,
      'verify_peer_name' => false,
      'allow_self_signed' => true
  )
);
// $mail->SMTPDebug = 1;
$mailContent = $html;
$mail->Body = $mailContent;
// if($mail->send()) {
//   echo 'OKe mail sent';
// } else {
//   echo 'err';
// }

$mpdf->WriteHTML(utf8_encode($html));
$mpdf->Output('laporan_keselamatan_'.$awal.'_sampai_'.$akhir.'.pdf', 'I');

?>