<?php
require_once('../koneksi.php');

// $npm = $_GET['npm'];
$page = 1;
$limit = 10;
$NAMA_ALAT = '';

function cekPeminjamanAlat ($idAlat) {
  $sqlAlat = mysql_query("SELECT SUM(jumlah_alat) as 'total_dipakai' FROM peminjaman_alat WHERE DATE(tanggal) = CURRENT_DATE() AND id_alat = '$idAlat'");
  $resultPeminjaman = mysql_fetch_assoc($sqlAlat);
  $sqlPengembalian = mysql_query("SELECT SUM(jumlah_alat) as 'total_dikembalikan' FROM pengembalian_alat WHERE DATE(tanggal) = CURRENT_DATE() AND id_alat = '$idAlat'");
  $resultPengembalian = mysql_fetch_assoc($sqlPengembalian);
  if(!$resultPeminjaman['total_dipakai'] || !$resultPengembalian['total_dikembalikan']) {
    return 0;
  } else {
    return $resultPeminjaman['total_dipakai'] - $resultPengembalian['total_dikembalikan'];
  }
}

if(isset($_GET['page'])) {
	$page = $_GET['page'];
}

if(isset($_GET['limit'])) {
	$limit = $_GET['limit'];
}

if(isset($_GET['NAMA_ALAT'])) {
  $NAMA_ALAT = $_GET['NAMA_ALAT'];
}
$skip = ($page - 1) * $limit;

$sql = "SELECT * FROM alat WHERE NAMA_ALAT LIKE '%$NAMA_ALAT%' LIMIT $limit OFFSET $skip";
$result = mysql_query($sql);

$sqlTotal = "SELECT COUNT(ID_ALAT) as totalRow FROM alat";
$resTotal = mysql_query($sqlTotal);

$rowcount = mysql_fetch_assoc($resTotal);

$output = array();

while($row = mysql_fetch_assoc($result)) {
  $dipakai = cekPeminjamanAlat($row['ID_ALAT']);
  // $row['sisa_alat'] = $row['JUMLAH_ALAT'] - (int) $dipakai;
  $row['dipakai'] = (int) $dipakai;
	$output[] = $row;
}

echo json_encode(array('results' => $output, 'total' => intval($rowcount['totalRow'])));
// print_r($output);
?>