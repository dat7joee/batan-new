<?php
require_once('../koneksi.php');
if(!$_GET['id']) {
  http_response_code(500);
  echo json_encode(array('success' => false, 'msg' => 'ID KOSONG!'));
  return;
}

$id = $_GET['id'];
$sql = "SELECT * FROM kegiatan LEFT JOIN alat ON kegiatan.ID_ALAT = alat.ID_ALAT WHERE ID = '$id'";
$run = mysql_query($sql);
$result = mysql_fetch_assoc($run);

if($run) {
  if($result['ID']) {
    $result['DOSIS_AWAL'] = intval($result['DOSIS_AWAL']);
    $result['DOSIS_AKHIR'] = intval($result['DOSIS_AKHIR']);
    http_response_code(200);
    echo json_encode(array('success' => true, 'kegiatan' => $result));
  } else {
    http_response_code(200);
    echo json_encode(array('success' => false));
  }
} else {
  http_response_code(500);
  echo json_encode(array('success' => false, 'err' => mysql_error()));
}

?>