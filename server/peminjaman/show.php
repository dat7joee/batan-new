<?php
require_once('../koneksi.php');
if(!$_GET['id']) {
  http_response_code(500);
  echo json_encode(array('success' => false, 'msg' => 'ID KOSONG!'));
  return;
}

$id = $_GET['id'];
// $sql = "SELECT * FROM peminjaman_alat LEFT JOIN alat ON peminjaman_alat.id_alat = alat.ID_ALAT WHERE id = '$id'";
$sql = "SELECT peminjaman_alat.*, alat.NAMA_ALAT as 'nama_alat', biodatapekerja.nama_lengkap FROM peminjaman_alat LEFT JOIN alat ON peminjaman_alat.id_alat = alat.ID_ALAT LEFT JOIN biodatapekerja ON peminjaman_alat.id_user = biodatapekerja.id WHERE peminjaman_alat.id = '$id'";
$run = mysql_query($sql);
$result = mysql_fetch_assoc($run);
if($run) {
  if($result['id']) {
    $result['jumlah_alat'] = (int) $result['jumlah_alat'];
    http_response_code(200);
    echo json_encode(array('success' => true, 'peminjaman' => $result));
  } else {
    http_response_code(200);
    echo json_encode(array('success' => false));
  }
} else {
  http_response_code(500);
  echo json_encode(array('success' => false, 'err' => mysql_error()));
}

?>