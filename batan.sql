-- phpMyAdmin SQL Dump
-- version 3.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 26, 2018 at 12:35 PM
-- Server version: 5.5.25a
-- PHP Version: 5.4.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `batan`
--

-- --------------------------------------------------------

--
-- Table structure for table `alat`
--

CREATE TABLE IF NOT EXISTS `alat` (
  `ID_ALAT` varchar(50) NOT NULL,
  `NAMA_ALAT` varchar(50) NOT NULL,
  `JUMLAH_ALAT` int(10) NOT NULL,
  `TANGGAL` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `alat`
--

INSERT INTO `alat` (`ID_ALAT`, `NAMA_ALAT`, `JUMLAH_ALAT`, `TANGGAL`) VALUES
('AL02913', 'Bom Atom', 5, '2018-07-25');

-- --------------------------------------------------------

--
-- Table structure for table `biodatapekerja`
--

CREATE TABLE IF NOT EXISTS `biodatapekerja` (
  `id` int(11) DEFAULT NULL,
  `nama_lengkap` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `ttl` date DEFAULT NULL,
  `alamat` text,
  `jabatan` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `biodatapekerja`
--

INSERT INTO `biodatapekerja` (`id`, `nama_lengkap`, `email`, `ttl`, `alamat`, `jabatan`) VALUES
(1, 'Jois Andresky', 'jois@klikfix.com', '1997-06-28', 'Cikarang', 'Web Developer'),
(2, 'Bagas', 'bagasprakoso3012@yahoo.com', '1995-12-31', 'Cileungsi', 'Babu');

-- --------------------------------------------------------

--
-- Table structure for table `datapekerja`
--

CREATE TABLE IF NOT EXISTS `datapekerja` (
  `ID_USER` int(10) NOT NULL,
  `TANGGAL` date NOT NULL,
  `KEGIATAN` varchar(50) NOT NULL,
  `JAM_MASUK` time NOT NULL,
  `JAM_KELUAR` time NOT NULL,
  `DOSIS_AWAL` int(10) NOT NULL,
  `DOSIS_AKHIR` int(10) NOT NULL,
  `RUANGAN` varchar(50) NOT NULL,
  `TYPE_DOSE` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `datapekerja`
--

INSERT INTO `datapekerja` (`ID_USER`, `TANGGAL`, `KEGIATAN`, `JAM_MASUK`, `JAM_KELUAR`, `DOSIS_AWAL`, `DOSIS_AKHIR`, `RUANGAN`, `TYPE_DOSE`) VALUES
(1, '2018-08-02', 'Ngebom', '02:00:00', '09:00:00', 120, 240, 'UTAMA', '2'),
(1, '2018-08-05', 'Input', '03:00:00', '09:00:00', 210, 220, 'TEST', 'TEST'),
(1, '2018-08-05', 'MEJENG', '05:00:00', '07:00:00', 190, 199, 'TEST', 'TEST'),
(1, '2018-08-05', 'sada', '18:17:30', '18:17:32', 220, 122, 'ASdas', '2'),
(2, '2018-08-09', 'TESTING', '08:00:00', '10:00:00', 120, 110, '5', '2');

-- --------------------------------------------------------

--
-- Table structure for table `kegiatan`
--

CREATE TABLE IF NOT EXISTS `kegiatan` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `TANGGAL` date NOT NULL,
  `NAMA` varchar(50) NOT NULL,
  `RUANGAN` varchar(50) NOT NULL,
  `TYPE_DOSE` varchar(50) NOT NULL,
  `DOSIS_AWAL` int(50) NOT NULL,
  `DOSIS_AKHIR` int(50) NOT NULL,
  `ID_ALAT` varchar(50) NOT NULL,
  `LOKASI` varchar(50) NOT NULL,
  `KEGIATAN` varchar(50) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `kegiatan`
--

INSERT INTO `kegiatan` (`ID`, `TANGGAL`, `NAMA`, `RUANGAN`, `TYPE_DOSE`, `DOSIS_AWAL`, `DOSIS_AKHIR`, `ID_ALAT`, `LOKASI`, `KEGIATAN`) VALUES
(2, '2018-07-25', 'TEST 2', 'TEST', 'TEST', 2, 2, 'AL02913', 'JAUH', 'TESTING');

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE IF NOT EXISTS `login` (
  `id_user` int(11) NOT NULL,
  `username` varchar(15) NOT NULL,
  `status` varchar(20) NOT NULL,
  `password` varchar(15) NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`id_user`, `username`, `status`, `password`) VALUES
(0, 'admin', 'Admin', '123456'),
(1, 'jojo', 'Pekerja', '123'),
(2, 'bagas', 'Pekerja', '123');

-- --------------------------------------------------------

--
-- Table structure for table `peminjaman_alat`
--

CREATE TABLE IF NOT EXISTS `peminjaman_alat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_alat` varchar(50) NOT NULL,
  `jumlah_alat` int(11) NOT NULL DEFAULT '0',
  `id_user` int(11) NOT NULL,
  `tanggal` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `peminjaman_alat`
--

INSERT INTO `peminjaman_alat` (`id`, `id_alat`, `jumlah_alat`, `id_user`, `tanggal`) VALUES
(5, 'AL02913', 2, 1, '2018-08-25 00:00:00'),
(6, 'AL02913', 4, 2, '2018-08-25 00:00:00'),
(7, 'AL02913', 2, 1, '2018-08-26 17:33:47');

-- --------------------------------------------------------

--
-- Table structure for table `pengembalian_alat`
--

CREATE TABLE IF NOT EXISTS `pengembalian_alat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_alat` varchar(50) DEFAULT NULL,
  `jumlah_alat` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `pengembalian_alat`
--

INSERT INTO `pengembalian_alat` (`id`, `id_alat`, `jumlah_alat`, `id_user`, `tanggal`) VALUES
(2, 'AL02913', 2, 1, '2018-08-25 17:00:00'),
(3, 'AL02913', 4, 2, '2018-08-25 15:00:00'),
(4, 'AL02913', 2, 1, '2018-08-26 17:34:34');

-- --------------------------------------------------------

--
-- Table structure for table `waktu`
--

CREATE TABLE IF NOT EXISTS `waktu` (
  `TANGGAL` date NOT NULL,
  `NAMA` varchar(50) NOT NULL,
  `DOSIS_AWAL` int(50) NOT NULL,
  `DOSIS_AKHIR` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
