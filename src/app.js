angular.module('batan', ['ngTable', 'ui.router'])
  .constant('BASE_URL', 'http://localhost/batan-new')
  .config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('index', {
        url: '',
        templateUrl: '../src/templates/_main.html'
      })
  })
  .controller('MasterCtrl', function ($scope) {
    $scope.user = JSON.parse(localStorage.getItem('user'));
    $scope.onLogout = function () {
      localStorage.removeItem('user');
      localStorage.removeItem('loggedIn');
      window.open("../login.html", "_self");
    };
  })
  .controller('LoginCtrl', function ($scope, $http, BASE_URL) {
    $scope.user = {};
    $scope.onLogin = false;
    $scope.onSubmit = function () {
      $scope.onLogin = true;
      $http.post(BASE_URL + '/server/ceklogin.php', $scope.user).then(function (response) {
        console.log('response', response);
        if (response.data.success) {
          localStorage.setItem('user', JSON.stringify(response.data.user));
          localStorage.setItem('loggedIn', true);
          window.open("dashboard/", "_self");
        } else {
          alert(response.data.msg);
        }
        $scope.onLogin = false;
      }).catch(function (err) {
        alert('Ooppss...! Terjadi Masalah saat mengkoneksikan ke Server!');
        console.log('err', err);
        $scope.onLogin = false;
      });
    }
  });