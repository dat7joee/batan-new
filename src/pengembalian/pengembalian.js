angular.module('batan')
  .config(function ($stateProvider) {
    $stateProvider
      .state('pengembalian', {
        abstract: true,
        templateUrl: '../src/templates/_layout.html'
      })
      .state('pengembalian.index', {
        url: '/pengembalian-alat',
        templateUrl: '../src/pengembalian/pengembalian.html',
        controller: 'PengembalianCtrl'
      })
      .state('pengembalian.new', {
        url: '/pengembalian-alat/new',
        templateUrl: '../src/pengembalian/form/pengembalian.html',
        controller: 'PengembalianFormCtrl'
      });
  });