angular.module('batan')
  .controller('PengembalianFormCtrl', function ($scope, $http, $state, $stateParams, BASE_URL) {
    $scope.model = {};
    $scope.pekerja = [];
    $scope.onEdit = false;
    if ($stateParams.id) {
      $scope.onEdit = true;
      $http.get(BASE_URL + '/server/pengembalian/show.php?id=' + $stateParams.id).then(function (response) {
        console.log('res', response.data.success);
        if (response.data.success) {
          $scope.model = response.data.pengembalian;
        } else {
          console.log('ress not found', response);
          alert('Oopss..! Data Tidak Ditemukan');
          // $state.go('^.index');
        }
      }).catch(function (err) {
        // $state.go('^.index');
        console.log('err', err);
        alert('Oopss!! Data Tidak Ditemukan!');
      });
    }

    $scope.onSave = function () {
      if (confirm('Apakah Kamu Yakin Ingin Menyimpan Data Ini ?')) {
        if (!$stateParams.id) {
          var user = JSON.parse(localStorage.getItem('user'));
          $http.post(BASE_URL + '/server/pengembalian/insert.php', $scope.model).then(function (response) {
            if (response.data.success) {
              alert('Berhasil Menyimpan!');
              $state.go('^.index');
            } else {
              alert(response.data.msg);
            }
          }).catch(function (err) {
            console.log(err);
            alert('Oopss! Error Terjadi Ketika Menghubungi Server!');
          });
        } else {
          $http.post(BASE_URL + '/server/pengembalian/update.php?id=' + $stateParams.id, $scope.model).then(function (response) {
            alert('Berhasil Menyimpan!');
            $state.go('^.index');
          }).catch(function (err) {
            console.log(err);
            alert('Oopss! Error Terjadi Ketika Menghubungi Server!');
          });
        }
      }
    };

    $scope.getPegawai = function (value) {
      $http.get(BASE_URL + '/server/biodata/search.php').then(function (response) {
        $scope.pekerja = response.data;
      }).catch(function (err) {
        console.log(err);
      });
    }
  });