angular.module('batan')
  .controller('KegiatanCtrl', function ($scope, $http, NgTableParams, $state, BASE_URL) {

    $scope.table = new NgTableParams({
      count: 10,
      page: 1
    }, { getData: getData });

    function getData(params) {
      var name = params.filter().NAMA ? params.filter().NAMA : '';
      var alat = params.filter().ID_ALAT ? params.filter().ID_ALAT : '';
      return $http.get(BASE_URL + '/server/kegiatan/data.php?page=' + params.page() + '&limit=' + params.count() + '&NAMA=' + name + '&ID_ALAT=' + alat).then(function (response) {
        console.log('res', response);
        params.total(response.data.total);
        return response.data.results;
      }).catch(function (err) {
        if (err) console.log(err);
      });
    }

    $scope.onDelete = function (id, kegiatan) {
      if (confirm('Apakah Kamu Yakin Ingin Menghapus Data Ini ? ' + kegiatan.NAMA_ALAT)) {
        $http.get(BASE_URL + '/server/kegiatan/delete.php?id=' + id).then(function (response) {
          alert('Berhasil Menghapus Data!');
          $state.reload();
        }).catch(function (err) {
          console.log(err);
          alert('Oopss!! Ada Kesalahan Saat Menghapus Data Dari Server!');
        });
      }
    };

  });