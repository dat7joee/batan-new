angular.module('batan')
  .config(function ($stateProvider) {
    $stateProvider
      .state('kegiatan', {
        abstract: true,
        templateUrl: '../src/templates/_layout.html'
      })
      .state('kegiatan.index', {
        url: '/kegiatan',
        templateUrl: '../src/kegiatan/kegiatan.html',
        controller: 'KegiatanCtrl'
      })
      .state('kegiatan.new', {
        url: '/kegiatan/new',
        templateUrl: '../src/kegiatan/form/kegiatan.html',
        controller: 'KegiatanFormCtrl'
      })
      .state('kegiatan.edit', {
        url: '/kegiatan/edit/:id',
        templateUrl: '../src/kegiatan/form/kegiatan.html',
        controller: 'KegiatanFormCtrl'
      });
  });