angular.module('batan')
  .controller('AlatCtrl', function ($scope, $http, NgTableParams, $state, BASE_URL) {

    $scope.table = new NgTableParams({
      count: 10,
      page: 1
    }, { getData: getData });

    function getData(params) {
      var name = params.filter().NAMA_ALAT ? params.filter().NAMA_ALAT : '';
      return $http.get(BASE_URL + '/server/alat/data.php?page=' + params.page() + '&limit=' + params.count() + '&NAMA_ALAT=' + name).then(function (response) {
        console.log('res', response);
        params.total(response.data.total);
        return response.data.results;
      }).catch(function (err) {
        if (err) console.log(err);
      });
    }

    $scope.onDelete = function (id, alat) {
      if (confirm('Apakah Kamu Yakin Ingin Menghapus Data Ini ? ' + alat.NAMA_ALAT)) {
        $http.get(BASE_URL + '/server/alat/delete.php?id=' + id).then(function (response) {
          alert('Berhasil Menghapus Data!');
          $state.reload();
        }).catch(function (err) {
          console.log(err);
          alert('Oopss!! Ada Kesalahan Saat Menghapus Data Dari Server!');
        });
      }
    };

  });