angular.module('batan')
  .config(function ($stateProvider) {
    $stateProvider
      .state('alat', {
        abstract: true,
        templateUrl: '../src/templates/_layout.html'
      })
      .state('alat.index', {
        url: '/alat',
        templateUrl: '../src/alat/alat.html',
        controller: 'AlatCtrl'
      })
      .state('alat.new', {
        url: '/alat/new',
        templateUrl: '../src/alat/form/alat.html',
        controller: 'AlatFormCtrl'
      })
      .state('alat.edit', {
        url: '/alat/edit/:id',
        templateUrl: '../src/alat/form/alat.html',
        controller: 'AlatFormCtrl'
      });
  });