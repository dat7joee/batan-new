angular.module('batan')
  .controller('AlatFormCtrl', function ($scope, $http, $state, $stateParams, BASE_URL) {
    $scope.model = {};
    $scope.onEdit = false;
    if ($stateParams.id) {
      $scope.onEdit = true;
      $http.get(BASE_URL + '/server/alat/show.php?id=' + $stateParams.id).then(function (response) {
        if (response.data.success) {
          $scope.model = response.data.alat;
        } else {
          console.log('ress not found', response);
          alert('Oopss..! Data Tidak Ditemukan');
          // $state.go('^.index');
        }
      }).catch(function (err) {
        // $state.go('^.index');
        console.log('err', err);
        alert('Oopss!! Data Tidak Ditemukan!');
      });
    }

    $scope.onSave = function () {
      if (confirm('Apakah Kamu Yakin Ingin Menyimpan Data Ini ?')) {
        if (!$stateParams.id) {
          $http.post(BASE_URL + '/server/alat/insert.php', $scope.model).then(function (response) {
            alert('Berhasil Menyimpan!');
            $state.go('^.index');
          }).catch(function (err) {
            console.log(err);
            alert('Oopss! Error Terjadi Ketika Menghubungi Server!');
          });
        } else {
          $http.post(BASE_URL + '/server/alat/update.php?id=' + $stateParams.id, $scope.model).then(function (response) {
            alert('Berhasil Menyimpan!');
            $state.go('^.index');
          }).catch(function (err) {
            console.log(err);
            alert('Oopss! Error Terjadi Ketika Menghubungi Server!');
          });
        }
      }
    };
  });