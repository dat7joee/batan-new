angular.module('batan')
  .config(function ($stateProvider) {
    $stateProvider
      .state('peminjaman', {
        abstract: true,
        templateUrl: '../src/templates/_layout.html'
      })
      .state('peminjaman.index', {
        url: '/peminjaman-alat',
        templateUrl: '../src/peminjaman/peminjaman.html',
        controller: 'PeminjamanCtrl'
      })
      .state('peminjaman.new', {
        url: '/peminjaman-alat/new',
        templateUrl: '../src/peminjaman/form/peminjaman.html',
        controller: 'PeminjamanFormCtrl'
      })
      .state('peminjaman.edit', {
        url: '/peminjaman-alat/edit/:id',
        templateUrl: '../src/peminjaman/form/peminjaman.html',
        controller: 'PeminjamanFormCtrl'
      });
  });