angular.module('batan')
  .controller('PeminjamanCtrl', function ($scope, $http, NgTableParams, $state, BASE_URL) {

    $scope.table = new NgTableParams({
      count: 10,
      page: 1
    }, { getData: getData });

    function getData(params) {
      var id_alat = params.filter().id_alat ? params.filter().id_alat : '';
      return $http.get(BASE_URL + '/server/peminjaman/data.php?page=' + params.page() + '&limit=' + params.count() + '&ID_ALAT=' + id_alat).then(function (response) {
        console.log('res', response);
        params.total(response.data.total);
        return response.data.results;
      }).catch(function (err) {
        if (err) console.log(err);
      });
    }

    $scope.onDelete = function (id, peminjaman) {
      if (confirm('Apakah Kamu Yakin Ingin Menghapus Data Ini ? ' + peminjaman.NAMA_ALAT)) {
        $http.get(BASE_URL + '/server/peminjaman/delete.php?id=' + id).then(function (response) {
          alert('Berhasil Menghapus Data!');
          $state.reload();
        }).catch(function (err) {
          console.log(err);
          alert('Oopss!! Ada Kesalahan Saat Menghapus Data Dari Server!');
        });
      }
    };

  });