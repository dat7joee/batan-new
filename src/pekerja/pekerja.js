angular.module('batan')
  .config(function ($stateProvider) {
    $stateProvider
      .state('pekerja', {
        abstract: true,
        templateUrl: '../src/templates/_layout.html'
      })
      .state('pekerja.index', {
        url: '/pekerja',
        templateUrl: '../src/pekerja/pekerja.html',
        controller: 'PekerjaCtrl'
      })
      .state('pekerja.new', {
        url: '/pekerja/new',
        templateUrl: '../src/pekerja/form/pekerja.html',
        controller: 'PekerjaFormCtrl'
      })
      .state('pekerja.edit', {
        url: '/pekerja/edit/:id',
        templateUrl: '../src/pekerja/form/pekerja.html',
        controller: 'PekerjaFormCtrl'
      });
  });