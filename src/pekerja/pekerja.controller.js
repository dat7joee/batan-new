angular.module('batan')
  .controller('PekerjaCtrl', function ($scope, $http, NgTableParams, $state, BASE_URL) {
    $scope.mailing = false;

    $scope.table = new NgTableParams({
      count: 10,
      page: 1
    }, { getData: getData });

    function getData(params) {
      var user = JSON.parse(localStorage.getItem('user'));
      var name = params.filter().NAMA ? params.filter().NAMA : '';
      var tanggal = params.filter().tanggal ? params.filter().tanggal : '';
      var id_user = '';
      if (user.status === 'Pekerja') {
        id_user = user.id_user;
      }
      return $http.get(BASE_URL + '/server/pekerja/data.php?page=' + params.page() + '&limit=' + params.count() + '&NAMA=' + name + '&user=' + id_user + '&tanggal=' + tanggal).then(function (response) {
        console.log('res', response);
        params.total(response.data.total);
        return response.data.results;
      }).catch(function (err) {
        if (err) console.log(err);
      });
    }

    $scope.onDelete = function (id, pekerja) {
      if (confirm('Apakah Kamu Yakin Ingin Menghapus Data Ini ? ' + pekerja.NAMA)) {
        $http.get(BASE_URL + '/server/pekerja/delete.php?id=' + id).then(function (response) {
          alert('Berhasil Menghapus Data!');
          $state.reload();
        }).catch(function (err) {
          console.log(err);
          alert('Oopss!! Ada Kesalahan Saat Menghapus Data Dari Server!');
        });
      }
    };

    $scope.sendEmail = function () {
      $scope.mailing = true;
      if (confirm('Kamu akan mengirimi email ke pada setiap pekerja ?')) {
        $http.get(BASE_URL + '/server/pekerja/mail-pekerja.php').then(function (response) {
          $scope.mailing = false;
          alert('Berhasil Mengirim Email!');
          $state.reload();
        }).catch(function (err) {
          console.log(err);
          alert('Oopss!! Ada Kesalahan Saat Mengirim Email Dari Server!');
        });
      } else {
        $scope.mailing = false;
      }
    };

    $scope.onPrint = function () {
      console.log('table', $scope.table.filter());
      var user = JSON.parse(localStorage.getItem('user'));
      var name = $scope.table.filter().NAMA ? $scope.table.filter().NAMA : '';
      var tanggal = $scope.table.filter().tanggal ? $scope.table.filter().tanggal : '';
      var id_user = '';
      if (user.status === 'Pekerja') {
        id_user = user.id_user;
      }
      window.open(BASE_URL + '/server/pekerja/report.php?NAMA=' + name + '&user=' + id_user + '&tanggal=' + tanggal, '_blank');
    }

  });