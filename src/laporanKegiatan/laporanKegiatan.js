angular.module('batan')
  .config(function ($stateProvider) {
    $stateProvider
      .state('laporanKegiatan', {
        abstract: true,
        templateUrl: '../src/templates/_layout.html'
      })
      .state('laporanKegiatan.index', {
        url: '/laporanKegiatan',
        templateUrl: '../src/laporanKegiatan/laporanKegiatan.html',
        controller: 'LaporanKegiatanCtrl'
      })
  });