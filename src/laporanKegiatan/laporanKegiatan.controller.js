angular.module('batan')
  .controller('LaporanKegiatanCtrl', function ($scope, $http, NgTableParams, $state, BASE_URL) {
    $scope.model = {};
    $scope.onPROCESS = function () {
      if (!$scope.model.awal && !$scope.model.akhir) {
        alert('Data Tidak Lengkap');
        return;
      }
      var awalDate = new Date($scope.model.awal);
      var akhirDate = new Date($scope.model.akhir);
      var awal = awalDate.getFullYear() + '-' + (awalDate.getMonth() + 1) + '-' + awalDate.getDate();
      var akhir = akhirDate.getFullYear() + '-' + (akhirDate.getMonth() + 1) + '-' + akhirDate.getDate();
      window.open(BASE_URL + '/server/pekerja/report-kegiatan.php?awal=' + awal + '&akhir=' + akhir, "_blank");
    };

  });