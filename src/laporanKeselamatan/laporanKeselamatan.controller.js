angular.module('batan')
  .controller('LaporanKeselamatanCtrl', function ($scope, $http, NgTableParams, $state, BASE_URL) {
    $scope.model = {};
    $scope.pekerja = [];
    $scope.onPROCESS = function () {
      if (!$scope.model.awal && !$scope.model.akhir) {
        alert('Data Tidak Lengkap');
        return;
      }
      var user = JSON.parse(localStorage.getItem('user'));
      if (user.status === 'Pekerja') {
        $scope.model.pekerja = user.id_user;
      }
      var awalDate = new Date($scope.model.awal);
      var akhirDate = new Date($scope.model.akhir);
      var awal = awalDate.getFullYear() + '-' + (awalDate.getMonth() + 1) + '-' + awalDate.getDate();
      var akhir = akhirDate.getFullYear() + '-' + (akhirDate.getMonth() + 1) + '-' + akhirDate.getDate();
      window.open(BASE_URL + '/server/pekerja/report-keselamatan.php?pekerja=' + $scope.model.pekerja + '&awal=' + awal + '&akhir=' + akhir, "_blank");
    };

    $scope.getPegawai = function (value) {
      $http.get(BASE_URL + '/server/biodata/search.php').then(function (response) {
        $scope.pekerja = response.data;
      }).catch(function (err) {
        console.log(err);
      });
    }

  });