angular.module('batan')
  .config(function ($stateProvider) {
    $stateProvider
      .state('laporanKeselamatan', {
        abstract: true,
        templateUrl: '../src/templates/_layout.html'
      })
      .state('laporanKeselamatan.index', {
        url: '/laporanKeselamatan',
        templateUrl: '../src/laporanKeselamatan/laporanKeselamatan.html',
        controller: 'LaporanKeselamatanCtrl'
      })
  });