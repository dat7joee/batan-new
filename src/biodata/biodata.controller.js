angular.module('batan')
  .controller('BiodataCtrl', function ($scope, $http, NgTableParams, $state, BASE_URL) {
    var user = JSON.parse(localStorage.getItem('user'));
    $scope.table = new NgTableParams({
      count: 10,
      page: 1
    }, { getData: getData });

    function getData(params) {
      var name = params.filter().nama_lengkap ? params.filter().nama_lengkap : '';
      return $http.get(BASE_URL + '/server/biodata/data.php?page=' + params.page() + '&limit=' + params.count() + '&NAMA=' + name).then(function (response) {
        console.log('res', response);
        params.total(response.data.total);
        return response.data.results;
      }).catch(function (err) {
        if (err) console.log(err);
      });
    }

    $scope.onDelete = function (id, biodata) {
      if (confirm('Apakah Kamu Yakin Ingin Menghapus Data Ini ? ' + biodata.NAMA)) {
        $http.get(BASE_URL + '/server/biodata/delete.php?id=' + id).then(function (response) {
          alert('Berhasil Menghapus Data!');
          $state.reload();
        }).catch(function (err) {
          console.log(err);
          alert('Oopss!! Ada Kesalahan Saat Menghapus Data Dari Server!');
        });
      }
    };
  });