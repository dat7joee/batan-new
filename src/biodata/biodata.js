angular.module('batan')
  .config(function ($stateProvider) {
    $stateProvider
      .state('biodata', {
        abstract: true,
        templateUrl: '../src/templates/_layout.html'
      })
      .state('biodata.index', {
        url: '/biodata',
        templateUrl: '../src/biodata/biodata.html',
        controller: 'BiodataCtrl'
      })
      .state('biodata.new', {
        url: '/biodata/new',
        templateUrl: '../src/biodata/form/biodata.html',
        controller: 'BiodataFormCtrl'
      })
      .state('biodata.edit', {
        url: '/biodata/edit/:id',
        templateUrl: '../src/biodata/form/biodata.html',
        controller: 'BiodataFormCtrl'
      })
      .state('biodata.profile', {
        url: '/biodata/profile',
        templateUrl: '../src/biodata/profile.html',
        controller: 'ProfileCtrl'
      });
  });