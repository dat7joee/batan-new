angular.module('batan')
  .controller('BiodataFormCtrl', function ($scope, $http, $state, $stateParams, BASE_URL) {
    $scope.model = {};
    $scope.onEdit = false;
    if ($stateParams.id) {
      var user = JSON.parse(localStorage.getItem('user'));

      if (user.status === 'Pekerja' && $stateParams.id !== user.id_user) {
        $state.go('^.profile');
        return;
      }
      $scope.onEdit = true;
      $http.get(BASE_URL + '/server/biodata/show.php?id=' + $stateParams.id).then(function (response) {
        if (response.data.success) {
          $scope.model = response.data.biodata;
        } else {
          console.log('ress not found', response);
          alert('Oopss..! Data Tidak Ditemukan');
          // $state.go('^.index');
        }
      }).catch(function (err) {
        // $state.go('^.index');
        console.log('err', err);
        alert('Oopss!! Data Tidak Ditemukan!');
      });
    }

    $scope.onSave = function () {
      if (confirm('Apakah Kamu Yakin Ingin Menyimpan Data Ini ?')) {
        if (!$stateParams.id) {
          $http.post(BASE_URL + '/server/biodata/insert.php', $scope.model).then(function (response) {
            alert('Berhasil Menyimpan!');
            $state.go('^.index');
          }).catch(function (err) {
            console.log(err);
            alert('Oopss! Error Terjadi Ketika Menghubungi Server!');
          });
        } else {
          $http.post(BASE_URL + '/server/biodata/update.php?id=' + $stateParams.id, $scope.model).then(function (response) {
            alert('Berhasil Menyimpan!');
            var user = JSON.parse(localStorage.getItem('user'));
            if (user.status === 'Pekerja') {
              $state.go('^.profile');
            } else {
              $state.go('^.index');
            }
          }).catch(function (err) {
            console.log(err);
            alert('Oopss! Error Terjadi Ketika Menghubungi Server!');
          });
        }
      }
    };
  });