angular.module('batan')
  .controller('ProfileCtrl', function ($scope, $http, NgTableParams, $state, BASE_URL) {
    var user = JSON.parse(localStorage.getItem('user'));
    $scope.pekerja = {};
    if (!user) {
      $state.go('index');
    } else {
      $http.get(BASE_URL + '/server/biodata/show.php?id=' + user.id_user).then(function (response) {
        console.log('response', response);
        $scope.pekerja = response.data.biodata;
      }).catch(function (err) {
        console.log('err', err);
      });
    }

  });